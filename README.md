# Case Cleanup Automation

## Overview

An org configuration that cleans up inactive cases (open cases with no actions in the last 100 days) and informs the case owners via email/chatter automatically each night at midnight.

## Some Screenshots

### Cases Closed Email

The 20 cases in the below screenshots all belonged to the same user, so they see quite a long list of hyperlinks.

![Email After Case Closed](media/closed-email.png)

### Cases Closed Chatter Post

![Cases Closed Chatter Post](media/closed-chatter.png)

## File Overview

### Apex

* **CaseCleanupBatchable** - Workhorse class that fetches inactive cases (open cases with 100+ days of no activity), closes them, then sends an email/makes a chatter post to the owner of the closed case with a list of all their cases that were closed.
* **CaseCleanupSchedulable** - Schedulable that simply executes CaseCleanupBatchable.
* **CaseCleanupHelper** - Helper class used by CaseCleanupBatchable to construct email and chatter objects that will be sent to relevant case owners.
* **CaseCleanupEmailBatchable** - Batchable executed if the number of emails (aka number of relevant case owners) will exceed the limit of a single Messaging.sendEmail call. Sends emails in batches of 100. 
* **CaseCleanupEmailData** - Wrapper used by the batchables. Contains info about each relevant case owner and the closed cases associated with them.
