@isTest
public with sharing class CaseCleanup_Test {

    private static Integer NUM_STALE_CASES = 50; // "web" cases
    private static Integer NUM_ACTIVE_CASES = 50; // "phone" cases
    private static DateTime STALE_DATE = DateTime.now().addDays(-150);
    private static Integer NUM_OF_USERS = 1;

    @TestSetup
    static void makeData(){
        List<Case> cases = new List<Case>();
        // Inactive (older than 100 days)
        for(Integer i=0; i < NUM_STALE_CASES; i++) {
            cases.add(new Case(CreatedDate=STALE_DATE.addDays(-1), LastModifiedDate=STALE_DATE));
        }
        // Active (newer than 100 days)
        for(Integer i=0; i < NUM_ACTIVE_CASES; i++) {
            cases.add(new Case());
        }
        insert cases;
    }
    
    @isTest
    static void doesBatchableCloseStaleCases()
    {
        Integer numActiveCasesStart = [SELECT COUNT() FROM Case
                                  WHERE IsClosed = False];
        System.debug('Active cases: ' + numActiveCasesStart);

        Test.startTest();
        CaseCleanupBatchable batchable = new CaseCleanupBatchable();
        Database.executeBatch(batchable);
        Test.stopTest();

        Integer numActiveCasesStop = [SELECT COUNT() FROM Case
                                  WHERE IsClosed = False];
        System.assertEquals(numActiveCasesStart - NUM_STALE_CASES, numActiveCasesStop, 'Incorrect number of cases were closed.');
    }

    @isTest
    static void doesBatchablePostToChatter()
    {
        Integer numFeedItemsStart = [SELECT COUNT() FROM FeedItem];
        System.debug('Feed items: ' + numFeedItemsStart);

        Test.startTest();
        CaseCleanupBatchable batchable = new CaseCleanupBatchable();
        Database.executeBatch(batchable);
        Test.stopTest();

        Integer numFeedItemsStop = [SELECT COUNT() FROM FeedItem];
        System.assertEquals(numFeedItemsStart + NUM_OF_USERS, numFeedItemsStop, 'A feed item was not posted for each relevent user.');
    }

    @isTest
    static void doesSchedulableScheduleBatchable()
    {
        String cronExpr = '0 0 0 ? * * 2076';
        Integer numOfJobsBefore = [SELECT COUNT() FROM AsyncApexJob];
        System.assertEquals(0, numOfJobsBefore, 'Not expecting any async jobs');
    
        Test.startTest();
        String jobId = System.schedule('testJob', cronExpr, new CaseCleanupSchedulable());
        Test.stopTest();
    
        List<AsyncApexJob> jobsScheduled = [SELECT Id, ApexClass.Name FROM AsyncApexJob WHERE JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'Expecting one scheduled job.');
        System.assertEquals('CaseCleanupSchedulable', jobsScheduled[0].ApexClass.Name, 'Scheduled job was not named "CaseCleanupSchedulable"');
    
        // check apex batch is in the job list
        List<AsyncApexJob> jobsApexBatch = [SELECT Id, ApexClass.Name FROM AsyncApexJob WHERE JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'Expecting one batch job');
        System.assertEquals('CaseCleanupBatchable', jobsApexBatch[0].ApexClass.Name, 'Batch job was not named "CaseCleanupBatchable"');
    }

    @isTest
    static void doesSchedulableCatchUnknownError()
    {
        Integer numActiveCasesStart = [SELECT COUNT() FROM Case
                                  WHERE IsClosed = False];
        String cronExpr = '0 0 0 ? * * 2076';

        Test.startTest();
        CaseCleanupSchedulable sch = new CaseCleanupSchedulable();
        sch.constructorException = true;
        String jobId = System.schedule('testJob', cronExpr, sch);
        Test.stopTest();

        Integer numActiveCasesEnd = [SELECT COUNT() FROM Case
                                  WHERE IsClosed = False];
        System.assertEquals(numActiveCasesStart, numActiveCasesEnd, 'Exception was not thrown. Cases were updated.');
    }

    @isTest
    static void doesBatchableLogExceptions()
    {
        Integer numActiveCasesStart = [SELECT COUNT() FROM Case
                                  WHERE IsClosed = False];
        Integer numLoggedErrorsStart = [SELECT COUNT() FROM Error_Log__c];
        CaseCleanupBatchable batchable = new CaseCleanupBatchable();
        batchable.executeException = true;
        batchable.finishException = true;

        Test.startTest();
        Database.executeBatch(batchable);
        Test.stopTest();

        Integer numActiveCasesEnd = [SELECT COUNT() FROM Case
                                  WHERE IsClosed = False];
        Integer numLoggedErrorsEnd = [SELECT COUNT() FROM Error_Log__c];
        System.assertEquals(numActiveCasesStart, numActiveCasesEnd, 'Exception was not thrown. Cases were updated.');
        System.assertNotEquals(numLoggedErrorsStart, numLoggedErrorsEnd, 'No error was logged.');

    }

    @isTest
    static void doesCaseCleanupEmailBatchableExecute()
    {
        Id senderId = null;
        CaseCleanupEmailData sampleEmailData = new CaseCleanupEmailData(UserInfo.getUserId(), 'recName');
        Test.startTest();
        Database.executeBatch(new CaseCleanupEmailBatchable(new List<CaseCleanupEmailData>{sampleEmailData}, senderId), 100);
        Test.stopTest();
    }
}
