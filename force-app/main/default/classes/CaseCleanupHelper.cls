public with sharing class CaseCleanupHelper {
    public static String BASE_ORG_URL = System.URL.getSalesforceBaseUrl().toExternalForm();

    /*
        Returns a single Messaging.SingleEmailMessage with settings based on the passed parameters

        Parameters:
        @    emailDetails - Info needed to fill out email (Owner Id/Name, closed cases associated with owner)
        @    senderId - Id of email address to be sent from
    */
    public static Messaging.SingleEmailMessage generateOutboundEmail(CaseCleanupEmailData emailDetails, Id senderId)
    {
        Messaging.SingleEmailMessage outboundEmail = new Messaging.SingleEmailMessage();
        outboundEmail.setTargetObjectId(emailDetails.ownerId);
        outboundEmail.setTreatTargetObjectAsRecipient(true);
        outboundEmail.setSaveAsActivity(false);
        outboundEmail.setUseSignature(false); 
        outboundEmail.setSubject(CaseCleanupHelper.buildEmailSubject(emailDetails.caseIdByCaseNumber.keySet().size()));
        outboundEmail.setHtmlBody(CaseCleanupHelper.buildEmailBody(emailDetails));
        if(senderId != null) {
            outboundEmail.setOrgWideEmailAddressId(senderId);
        }
        return outboundEmail;
    }

    /*
        Returns a string containing the email's subject line

        Parameters:
        @    numOfCases - How many cases associated with the owner were closed
    */
    public static String buildEmailSubject(Integer numOfCases)
    {
        return numOfCases > 1 ? 'Closed Inactive Cases' : 'Closed Inactive Case';
    }

    /*
        Returns a string containing the html version of the email's body

        Parameters:
        @    emailData - Contains owner name and closed cases associated with them
    */
    public static String buildEmailBody(CaseCleanupEmailData emailData) {
        List<String> caseLines = new List<String>();
        for(String eachCaseNumber : emailData.caseIdByCaseNumber.keySet()) {
            String caseLine = '<li><a href=' +
                                BASE_ORG_URL + '/' + emailData.caseIdByCaseNumber.get(eachCaseNumber) +
                                '>Case ' + eachCaseNumber + '</a></li>';
            caseLines.add(caseLine);
        }
        String body = 'Dear ' + emailData.ownerName + ',<br><br>' +
                'Due to inactivity, the following cases have been closed: <br><br>' + 
                '<ul>' + String.join(caseLines, '') + '</ul><br>' +
                '<b>This is an automated, unmonitored email. If you have questions, please contact your administrator.</b>';
        return body;
    }

    /*
        Returns a single FeedItem (chatter post) with settings based on the passed parameter

        Parameters:
        @    chatterDetails - Info needed to fill out post (Owner Id, closed cases associated with owner)
    */
    public static FeedItem generateChatterPost(CaseCleanupEmailData chatterDetails)
    {
        FeedItem newPost = new FeedItem();
        newPost.ParentId = chatterDetails.ownerId;
        newPost.IsRichText = True;
        newPost.Body = buildChatterBody(chatterDetails);
        return newPost;
    }

    /*
        Returns a string containing the rich text version of the post's body

        Parameters:
        @    chatterDetails - Contains owner name and closed cases associated with them
    */
    public static String buildChatterBody(CaseCleanupEmailData chatterDetails)
    {
        List<String> caseLines = new List<String>();
        for(String eachCaseNumber : chatterDetails.caseIdByCaseNumber.keySet()) {
            String caseLine = '<li><a href="' +
                                BASE_ORG_URL + '/' + chatterDetails.caseIdByCaseNumber.get(eachCaseNumber) +
                                '">Case ' + eachCaseNumber + '</a></li>';
            caseLines.add(caseLine);
        }
        String body = 'Dear ' + chatterDetails.ownerName + ',<p></p><p></p>' +
                'Due to inactivity, the following cases have been closed: <p></p><p></p>' + 
                '<ul>' + String.join(caseLines, '') + '</ul><p></p>';
        return body;
    }

    /*
        Sends a email detailing all the errors passed in the parameters to the user who scheduled the job

        Parameters:
        @    errors - List of Exceptions that occurred while executing the Case Cleanup
        @    jobId - Id of the job that caused the errors
    */
    public static void sendErrorReportToRunner(List<Exception> errors, String jobId)
    {
        Messaging.SingleEmailMessage errorMail = new Messaging.SingleEmailMessage();
        errorMail.setSubject('Errors Occurred During Case Cleanup');
        errorMail.setTargetObjectId(UserInfo.getUserId());
        errorMail.setSaveAsActivity(false);
        errorMail.setHtmlBody(buildErrorReportBody(errors, jobId));
        // Messaging.sendEmail(new List<Messaging.Email> { errorMail });
    }

    /*
        Returns a string containing the html version of the error email's body

        Parameters:
        @    errors - List of Exceptions that occurred while executing the Case Cleanup
        @    jobId - Id of the job that caused the errors
    */
    private static String buildErrorReportBody(List<Exception> errors, String jobId)
    {
        List<String> errorLines = new List<String>();
        for(Exception eachError : errors) {
            String caseLine = '<li><b>' + eachError.getMessage() + ' => </b>' + eachError.getStackTraceString() + '</li>';
            errorLines.add(caseLine);
        }
        String body = 'The following error(s) occurred during case cleanup (JobId: ' + jobId + ')<br><br>' +
                    '<ul>' + errorLines + '</ul>';
        return body;
    }

    /*
        Returns Id of org-wide email address used to send these automated emails, or null if not found
    */
    public static Id getSenderId()
    {
        Org_Wide_Email__mdt custEmail = [SELECT Email__c FROM Org_Wide_Email__mdt WHERE Type__c = 'CaseCleanup' LIMIT 1];
        List<OrgWideEmailAddress> owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :custEmail.Email__c];
        if ( owea.size() > 0 ) {
            return owea.get(0).Id;
        } else {
            return null;
        }
    }
}
