public with sharing class CaseCleanupBatchable implements Database.Batchable<sObject>, Database.Stateful {
    public DateTime customTime;
    public String query;
    public Map<Id, CaseCleanupEmailData> emailDataByOwnerId = new Map<Id, CaseCleanupEmailData>();
    public String closeStatus;
    public List<Exception> errors = new List<Exception>();

    // Used in unit tests
    @TestVisible private Boolean executeException = false;
    @TestVisible private Boolean finishException = false;

    /*
        CONSTRUCTOR
        Sets up the query to retrieve non-closed cases with no activity in the last 100 days
    */
    public CaseCleanupBatchable()
    {
        query = 'SELECT Id, CaseNumber, LastModifiedDate, OwnerId, Owner.FirstName FROM Case ' +
                ' WHERE IsClosed = False ' + 
                ' AND LastModifiedDate < LAST_N_DAYS:100';
        closeStatus = [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = true LIMIT 1].MasterLabel;
    }

    /*
        CONSTRUCTOR
        Used for testing email functionality in non-test settings

        Parameters:
        @    passedTime - Cutoff time for "inactive" cases. In the actualy batchable, this
                            defaults to 100 days
    */
    // public CaseCleanupBatchable(DateTime passedTime)
    // {
    //     customTime = passedTime;
    //     query = 'SELECT Id, CaseNumber, LastModifiedDate, OwnerId, Owner.FirstName FROM Case ' +
    //             ' WHERE IsClosed = False ' + 
    //             ' AND LastModifiedDate < :customTime';
    //     closeStatus = [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = true LIMIT 1].MasterLabel;
    // }

    /*
        START
        Fetch cases via query defined by Constructor
    */
    public Database.QueryLocator start(Database.BatchableContext bc)
    {
        System.debug('START CaseCleanupBatchable');
        return Database.getQueryLocator(query);
    }

    /*
        EXECUTE
        Walks through each found case, extracts its information for use in creating the email/chatter posts,
        and then updates its status to closed.
    */
    public void execute(Database.BatchableContext bc, List<Case> cases)
    {
        System.debug('EXECUTE CaseCleanupBatchable');
        if(!(cases.size() > 0)) {
            return;
        }

        try{
            if(Test.isRunningTest()) {
                if(executeException) { throw new LoggedException(); }
            }

            for(Case eachCase : cases) {
                // Wrap up data we will use to populate emails/chatter posts in finish()
                if(emailDataByOwnerId.containsKey(eachCase.OwnerId)) {
                    CaseCleanupEmailData currentEmailData = emailDataByOwnerId.get(eachCase.OwnerId);
                    currentEmailData.caseIdByCaseNumber.put(eachCase.CaseNumber, eachCase.Id);
                    emailDataByOwnerId.put(eachCase.OwnerId, currentEmailData);
                } else {
                    CaseCleanupEmailData newEmailData = new CaseCleanupEmailData(eachCase.OwnerId, eachCase.Owner.FirstName);
                    newEmailData.caseIdByCaseNumber.put(eachCase.CaseNumber, eachCase.Id);
                    emailDataByOwnerId.put(eachCase.OwnerId, newEmailData);
                }
                // Set case as closed
                eachCase.Status = closeStatus;
            }
            // DML
            update cases;
        } catch(Exception e) {
            System.debug('Error while executing Case Cleanup: ' + e.getMessage());
            errors.add(e);
        }
    }

    /*
        FINISH
        For each case owner who had a case closed during execution, send an email and make a chatter post
        telling them which cases were closed
    */
    public void finish(Database.BatchableContext bc)
    {
        System.debug('FINISH CaseCleanupBatchable');
        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        List<FeedItem> newChatterPosts = new List<FeedItem>();

        try {
            if(Test.isRunningTest()) {
                if(finishException) { throw new LoggedException(); }
            }

            // Setup email messages/chatter objects for send/insert
            Id senderId = CaseCleanupHelper.getSenderId();
            for(CaseCleanupEmailData eachOwnerEmailData : emailDataByOwnerId.values()) {
                outboundEmails.add(CaseCleanupHelper.generateOutboundEmail(eachOwnerEmailData, senderId));
                newChatterPosts.add(CaseCleanupHelper.generateChatterPost(eachOwnerEmailData));
            }

            // Send emails to relevant case owners
            if(outboundEmails.size() > 100) {
                Database.executeBatch(new CaseCleanupEmailBatchable(emailDataByOwnerId.values(), senderId), 100);
            } else {
                // Messaging.sendEmail(outboundEmails);
            }

            // Create chatter posts to relevant case owners
            insert newChatterPosts;

        } catch(Exception e) {
            System.debug('Error while finishing Case Cleanup: ' + e.getMessage());
            errors.add(e);
        }

        // Send error report to job scheduler and log errors
        if(!errors.isEmpty()) {
            // Send email
            CaseCleanupHelper.sendErrorReportToRunner(errors, bc.getJobId());
            // Insert new error log
            String logMessage = '';
            for(Exception eachError : errors) {
                logMessage += eachError.getMessage() + ' => ' + eachError.getStackTraceString();
            }
            insert new Error_Log__c(
                    Type__c='Scheduled',
                    Job_Id__c=bc.getJobId(),
                    Message__c=logMessage
                );
        }
    }
}
