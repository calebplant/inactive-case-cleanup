global with sharing class CaseCleanupSchedulable implements Schedulable {

    global String DEFAULT_CRON_STR = '0 0 0 * * ?';  // Daily at mightnight

    // Used in unit testing
    @TestVisible private Boolean constructorException = false;
    
    /*
        EXECUTE
        Simply executes the CaseCleanupBatchable at the scheduled time
    */
    global void execute(SchedulableContext ctx) {
        System.debug('START CaseCleanupSchedulable');
        String jobId;

        try {
            if(Test.isRunningTest()) {
                if(constructorException) { throw new LoggedException(); }
            }
            CaseCleanupBatchable batchable = new CaseCleanupBatchable();
            jobId = Database.executeBatch(batchable);
            System.debug('Batchable started with id: ' + jobId);
        } catch(Exception e) {
            // Unexcepted error
            System.debug('Unknown error occurred: ' + e.getMessage());
            CaseCleanupHelper.sendErrorReportToRunner(new List<Exception>{e}, jobId);
        }
    }
}
