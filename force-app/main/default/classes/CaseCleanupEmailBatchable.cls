public with sharing class CaseCleanupEmailBatchable implements Database.Batchable<CaseCleanupEmailData>, Database.Stateful {

    // Messaging.SingleEmailMessage is not serializable, so must build messages inside of execute instead
    public List<CaseCleanupEmailData> emailsToSend;
    public Id senderId;

    /*
        CONSTRUCTOR
        Sends case closed emails to relevant case owners in batches of 100. Used if the number of emails
        to send in CaseCleanupBatchable will exceed its limit (100).

        Parameters:
        @    emails - Info needed to fill out email (Owner Id/Name, closed cases associated with owner)
        @    sender - Id of email address to be sent from
    */
    public CaseCleanupEmailBatchable(List<CaseCleanupEmailData> emails, Id sender)
    {
        emailsToSend = emails;
        senderId = sender;
    }

    /*
        START
        Fetch case closed email details passed in the Constructor
    */
    public Iterable<CaseCleanupEmailData> start(Database.BatchableContext bc)
    {
        System.debug('START CaseCleanupEmailBatchable');
        return emailsToSend;
    }
   
    /*
        EXECUTE
        Walks through each email detail and constructs a Messaging.SingleEmailMessage, then sends all the
        constructed emails
    */
    public void execute(Database.BatchableContext bc, List<CaseCleanupEmailData> scope){
        System.debug('EXECUTE CaseCleanupEmailBatchable');
        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        for(CaseCleanupEmailData eachOwnerEmailData : scope) {
            outboundEmails.add(CaseCleanupHelper.generateOutboundEmail(eachOwnerEmailData, senderId));
        }

        // Messaging.sendEmail(outboundEmails);
    }
    
    /*
        FINISH
    */
    public void finish(Database.BatchableContext bc)
    {
        System.debug('FINISH CaseCleanupEmailBatchable');
    }
}
