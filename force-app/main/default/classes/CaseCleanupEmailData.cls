public class CaseCleanupEmailData{
    public Id ownerId;
    public String ownerName;
    public Map<String, Id> caseIdByCaseNumber = new Map<String, Id>();

    public CaseCleanupEmailData(Id passedId, String passedName)
    {
        ownerId = passedId;
        ownerName = passedName;
    }
}